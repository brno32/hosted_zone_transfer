import click
import json

import pprint

from pathlib import Path


@click.command()
@click.option(
    "--path",
    prompt="Input file path",
    help="The person to greet.",
    type=Path,
    default="example_zone.json",
)
def transform(path: Path):
    """Transforms zone intput to zone output"""
    print(path.name)

    with open(path, "r") as file:
        file_input = json.loads(file.read())

    pprint.pprint(file_input)

    print()
    print("transforming")
    print()

    record_sets = file_input.pop("ResourceRecordSets")
    transformed = {"Comment": "string", "Changes": []}

    aliases = []

    for record_set in record_sets:
        if record_set["Type"] in ["SOA", "NS"]:
            continue
        # these need to be added last
        if "AliasTarget" in record_set:
            aliases.append({"Action": "CREATE", "ResourceRecordSet": record_set})
            continue
        transformed["Changes"].append(
            {"Action": "CREATE", "ResourceRecordSet": record_set}
        )

    transformed["Changes"] += aliases

    pprint.pprint(transformed)


if __name__ == "__main__":
    transform()
